# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_accessor :towers

  def initialize
    @towers = [ [3, 2, 1], [], [] ]
  end

  def valid_move?(from_tower, to_tower)
    unless @towers[from_tower].empty?
      if @towers[to_tower].empty?
        return true
      elsif @towers[from_tower][-1] < @towers[to_tower][-1]
        return true
      end
    end
    false
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      @towers[to_tower] << @towers[from_tower].pop
    end
  end

  def won?
    if @towers == [ [], [3, 2, 1], [] ] || @towers == [ [], [], [3, 2, 1] ]
      return true
    end
    false
  end

  def play
    self.intro

    until self.won?
      puts "Tower to move from?"
      from_tower = gets.chomp
      if from_tower == "v"
        self.render
      elsif from_tower == 't'
        self.tutorial
      elsif from_tower == "q"
        break
      else
        puts "Tower to move to?"
        to_tower = gets.chomp
        self.move(from_tower.to_i, to_tower.to_i)
        self.render
      end
    end

    puts "You win!" if self.won?
  end

  def render
    print "#{@towers}\n"
  end

  def intro
    puts "What is your name?"
    name = gets.chomp
    puts "Hello, #{name}! Do you know how to play Towers of Hanoi?
    (enter yes or no)"

    response = gets.chomp
    until response == "yes" || response == "no"
      puts "Sorry, I didn't understand that. Please enter yes or no."
      response = gets.chomp
    end

    if response == "yes"
      puts "Excellent. Enjoy the game! Remember to press 't' when the
      'Tower to move from?' prompt appears if you wish to see the tutorial.
      (press enter to continue)"
      gets
    else
      self.tutorial
    end
  end

  def tutorial
    puts "The goal is to move a stack of discs from one tower to another.
    (press enter to continue)"
    gets

    puts "The towers are depicted as follows: [ [3, 2, 1], [], [] ]
    Each array corresponds to a tower; the numbers correspond to discs.
    The numbers represent disc sizes with 1 being the smallest.
    (press enter to continue)"
    gets

    puts "The game is over when a new tower is formed, either
    [ [], [3, 2, 1], [] ] or [ [], [], [3, 2, 1] ]. Discs must be moved one
    at a time, and larger discs cannot be stacked on smaller discs. For
    example, [3, 1] and [2] are valid placements but [1, 2] and [1, 3] are not.
    (press enter to continue)"
    gets

    puts "Controls: 0, 1 and 2 to move discs.
    -0, 1, and 2 represent towers 1, 2 and 3, respectively.
    Enter your choice of towers when the
    'Tower to move from?' and 'Tower to move to?' prompts appear.
    -When the 'Tower to move from?' prompt appears you can instead enter
    the following: 'v' to view towers, 'q' to quit, or 't' for tutorial.
    Good Luck! (end of tutorial. press enter to continue)"
    gets
  end
end

# To run the game in the console un-comment the following lines:
# puts "Towers of Hanoi: Press enter to continue"
# gets
# game = TowersOfHanoi.new
# game.play
